export class Invoice {
    // client: string;
    // private detail: string;
    // public amount: number;

    constructor(
        readonly client: string,
        private detail: string,
        public amount: number,
    ) {
    }

    format() {
        return `${this.client} pays $${this.amount} for ${this.detail}`;
    }
}