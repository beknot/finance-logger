type StringOrNum = string | number;
type ObjName = { name: string, uid: StringOrNum };

const log = (uid: StringOrNum, item: string) => {
    console.log(`${item} has uid of ${uid}`);
}
const hello = ( user: ObjName ) => {
    console.log(`{$user.name} says hello`);
}

let greet: (a: string, b:string) => void;
greet = (name: string, hello: string) => {
    console.log(`${name} says ${hello}`);
}

let calc: (a: number, b: number, c: string) => number;
calc = (x: number, y: number, z: string): number => {
    if(z === 'add') {
        return x+y
    } else {
        return x-y
    }
}


let logg: (ob: {name: string, age: number} ) =>void;
logg = (jss: {name: string, age: number}) => {
    console.log(`${jss.name} is ${jss.age} yrs old `)
}

//////////////////////////////////////////////////////

