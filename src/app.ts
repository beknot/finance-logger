interface IsPerson {
    name: string;
    age: number;
    speak(a: string): void;
    spend(a: number): number;
}

const me: IsPerson = {
    name: 'Binod',
    age: 22,
    speak(text: string): void {
        console.log(text)
    },
    spend(amount: number): number {
        console.log('I spent', amount)
        return amount
    }
};

let someone: IsPerson;

const greetPerson = (person: IsPerson) => {
    console.log('Hello', person.name);
}

greetPerson(me);

console.log(me)
console.log(me)




import { Invoice } from './classes/invoice.js'

const form = document.querySelector('.new-item-form') as HTMLFormElement;
// console.log(form.children);

const type = document.querySelector('#type') as HTMLSelectElement;
const tofrom = document.querySelector('#tofrom') as HTMLInputElement;
const details = document.querySelector('#details') as HTMLInputElement;
const amount = document.querySelector('#amount') as HTMLInputElement;

form.addEventListener('submit', (e: Event) => {
    e.preventDefault();
    console.log(
        type.value,
        tofrom.value,
        details.value,
        amount.value,
        );
})


const invOne = new Invoice('Binod', 'webdev', 100)
const invTwo = new Invoice('Beknot', 'webJS', 200)

// console.log(invOne, invTwo);

let invoices: Invoice[] = [];
invoices.push(invOne)
invoices.push(invTwo)

// console.log(invoices);

invoices.forEach(inv=> {
    console.log(inv.client, inv.amount, inv.format());
})
