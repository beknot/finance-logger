const me = {
    name: 'Binod',
    age: 22,
    speak(text) {
        console.log(text);
    },
    spend(amount) {
        console.log('I spent', amount);
        return amount;
    }
};
let someone;
const greetPerson = (person) => {
    console.log('Hello', person.name);
};
greetPerson(me);
console.log(me);
console.log(me);
import { Invoice } from './classes/invoice.js';
const form = document.querySelector('.new-item-form');
// console.log(form.children);
const type = document.querySelector('#type');
const tofrom = document.querySelector('#tofrom');
const details = document.querySelector('#details');
const amount = document.querySelector('#amount');
form.addEventListener('submit', (e) => {
    e.preventDefault();
    console.log(type.value, tofrom.value, details.value, amount.value);
});
const invOne = new Invoice('Binod', 'webdev', 100);
const invTwo = new Invoice('Beknot', 'webJS', 200);
// console.log(invOne, invTwo);
let invoices = [];
invoices.push(invOne);
invoices.push(invTwo);
// console.log(invoices);
invoices.forEach(inv => {
    console.log(inv.client, inv.amount, inv.format());
});
