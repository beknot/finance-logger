"use strict";
const log = (uid, item) => {
    console.log(`${item} has uid of ${uid}`);
};
const hello = (user) => {
    console.log(`{$user.name} says hello`);
};
let greet;
greet = (name, hello) => {
    console.log(`${name} says ${hello}`);
};
let calc;
calc = (x, y, z) => {
    if (z === 'add') {
        return x + y;
    }
    else {
        return x - y;
    }
};
let logg;
logg = (jss) => {
    console.log(`${jss.name} is ${jss.age} yrs old `);
};
//////////////////////////////////////////////////////
